const ProductVariant = require('../classes/ProductVariant');
const CONSTANTS = require('../config/constants');
const CommonFunctions = require('../config/common_functions');

const selectors = {
  title: {
    alza: '.name',
    emag: '.card-v2-title',
    gigatron: '.item__name',
    tehnomanija: '.product-carousel--info p'
  },
  price: {
    alza: '.price-box__price',
    emag: '.product-new-price',
    gigatron: '.item__bottom__prices__price',
    tehnomanija: '.product-carousel--info-newprice'
  },
  rating: {
    alza: ($, data) => $(data).find('.star-rating-wrapper').attr('title')?.replace('Értékelés ', '') ?? 'N/A',
    emag: ($, data) => $(data).find('.star-rating-container .star-rating-text .average-rating').text() ?? 'N/A',
    gigatron: 'N/A',
    tehnomanija: 'N/A'
  },
  stock: {
    alza: ($, data) => $(data).find('.avlVal.avl0').length > 0 ? 'inStock' : 'notAvailable',
    emag: ($, data) => $(data).find('.text-availability-in_stock').length > 0
      ? 'inStock'
      : ($(data).find('.text-availability-out_of_stock').length > 0 ? 'notAvailable' : 'N/A'),
    gigatron: ($, data) => $(data).find('.item__bottom .item__bottom__cart').length > 0 ? 'inStock' : 'notAvailable',
    tehnomanija: ($, data) => $(data).find('.product-in-stock').length > 0 ? 'inStock' : 'notAvailable'
  },
  avatar: {
    alza: ($, data) => $(data).find('.js-box-image.box-image').attr('srcset').split(',').pop().trim(),
    emag: ($, data) => $(data).find('.card-v2-thumb-inner img').attr('src'),
    gigatron: ($, data) => $(data).find('a.item__image img').attr('src'),
    tehnomanija: ($, data) => $(data).find('.product-carousel--image lib-media-container picture img').attr('src')
  },
  product_url: {
    alza: 'a.name',
    emag: 'h2.card-v2-title-wrapper a.card-v2-title.js-product-url',
    gigatron: 'a.item__name',
    tehnomanija: '.product-carousel--href'
  },
  id: {
    alza: ($, data) => $(data).data('code'),
    emag: ($, data) => $(data).find('div.card-v2-atc.mrg-top-xxs form').data('id'),
    gigatron: ($, data) => $(data).data('id'),
    tehnomanija: ($, data) => $(data).find('.product-carousel--href').attr('href').split('-').pop()
  },
  description: {
    alza: ($, data) => $(data).find('div.Description').text(),
    emag: 'N/A',
    gigatron: 'N/A',
    tehnomanija: 'N/A'
  }
};

/**
 * Searches in the product name for storage and ram.
 *
 * @param {String} productName
 */
const getStorageAndRam = (productName) => {
  const data = { ram: 'N/A', storage: 'N/A' };
  productName = productName.toUpperCase().replace(/,/g, '');

  for (const currentStorage of CONSTANTS.CAPACITIES.STORAGE) {
    for (const currentRam of CONSTANTS.CAPACITIES.RAM) {
      for (const magnitude of CONSTANTS.CAPACITY_MAGNITUDE) {
        const patterns = [
          {
            pattern: `${currentRam} ${magnitude} / ${currentStorage} ${magnitude}`,
            objs: ['ram', 'storage']
          },
          { pattern: `${currentRam}${magnitude} / ${currentStorage}${magnitude}`, objs: ['ram', 'storage'] },
          { pattern: `${currentRam} ${magnitude}/${currentStorage} ${magnitude}`, objs: ['ram', 'storage'] },
          { pattern: `${currentRam}${magnitude}/${currentStorage}${magnitude}`, objs: ['ram', 'storage'] },
          { pattern: `${currentRam}${magnitude} /${currentStorage}${magnitude}`, objs: ['ram', 'storage'] },
          { pattern: `${currentRam} ${magnitude} /${currentStorage}${magnitude}`, objs: ['ram', 'storage'] },
          { pattern: `${currentRam} ${magnitude} /${currentStorage} ${magnitude}`, objs: ['ram', 'storage'] },
          { pattern: `${currentRam}/${currentStorage} ${magnitude}`, objs: ['ram', 'storage'] },
          { pattern: `${currentRam}/${currentStorage}${magnitude}`, objs: ['ram', 'storage'] },
          { pattern: `${currentStorage}${magnitude}`, objs: ['storage'] },
          { pattern: `${currentStorage} ${magnitude}`, objs: ['storage'] },
          { pattern: `${currentRam}${magnitude} RAM`, objs: ['ram'] },
          { pattern: `${currentRam} ${magnitude} RAM`, objs: ['ram'] },
          { pattern: `${currentRam}RAM`, objs: ['ram'] },
          { pattern: `${currentRam}${magnitude}`, objs: ['ram'] },
          { pattern: `${currentRam} ${magnitude}`, objs: ['ram'] }
        ];
        for (const pattern of patterns) {
          if (CommonFunctions.matchExact(pattern.pattern, productName)) {
            if (pattern.objs.includes('ram') && data.ram === 'N/A') {
              data.ram = currentRam + magnitude;
            }
            if (pattern.objs.includes('storage')) {
              data.storage = currentStorage + magnitude;
            }
          }
        }
      }
    }
  }
  return data;
};
/**
 * Extracts each product variant's data from the scrapped page.
 *
 * @param page {string} The shop's name (alza, emag...) .
 * @param $ {cheerio} The cheerio jquery instance.
 * @param products {cheerio} Array from 'product cards' from the fetched data.
 * @returns {*[ProductVariant]} Returns the array of the scraped product variants.
 */
const commonProductScraper = (page, $, products) => {
  const productsArray = [];

  for (const product of products) {
    let title = $(product).find(selectors.title[page]).text() || 'N/A';

    if (title === 'N/A') {
      continue;
    }

    for (const obj of scraperConfig[page].replace_strings) {
      const searchedStr = Object.keys(obj)[0];
      const replacementStr = obj[searchedStr];
      title = title.replace(new RegExp(searchedStr, 'i'), replacementStr);
    }
    title = title.replace(new RegExp(scraperConfig[page].illegal_strings, 'gi'), '').replace(/ +/g, ' ').trim();

    const priceRaw = $(product).find(selectors.price[page]).text();
    let price = parseInt(CONSTANTS.CURRENCY_NAMES.reduce((price, name) => {
      return price.replace(name, '');
    }, priceRaw).replace(/[.\s]+/g, '').trim()) || 0;
    price *= scraperConfig[page].currency.multiplier;
    const id = selectors.id[page]($, product) || 'N/A';
    const avatar = selectors.avatar[page]($, product) || 'N/A';
    const rating = typeof selectors.rating[page] === 'function'
      ? (selectors.rating[page]($, product) ?? 'N/A')
      : (selectors.rating[page] ?? 'N/A');
    const stock = selectors.stock[page]($, product) || 'N/A';
    const description = typeof selectors.description[page] === 'function'
      ? selectors.description[page]($, product)
      : 'N/A';
    let productUrl = $(product).find(selectors.product_url[page]).attr('href') ?? 'N/A';
    if (productUrl.startsWith('/')) {
      productUrl = scraperConfig[page].products_page_base_url + productUrl;
    }
    const storageData = getStorageAndRam(title);

    productsArray.push(
      new ProductVariant(
        title,
        parseFloat(price.toFixed(2)),
        id,
        avatar,
        rating,
        stock,
        description,
        productUrl,
        page,
        storageData.ram,
        storageData.storage
      )
    );
  }

  return productsArray;
};
const scraperConfig = {
  alza: {
    page_name: 'www.alza.hu',
    currency: CONSTANTS.CURRENCIES.HUF,
    products_page_url: 'https://www.alza.hu/mobiltelefonok/18843445.htm',
    products_page_base_url: 'https://www.alza.hu/mobiltelefonok/',
    search_page_url: 'https://www.alza.hu/search.htm?exps=',
    next_page_selector: '#pagerbottom a.next.fa.fa-chevron-right',
    products_selector: 'div.browsingitem.box',
    product_scraper: async ($, products) => commonProductScraper('alza', $, products),
    illegal_strings: [],
    replace_strings: []
  },
  emag: {
    page_name: 'www.emag.hu',
    currency: CONSTANTS.CURRENCIES.HUF,
    products_page_url: 'https://www.emag.hu/mobiltelefonok/c',
    products_page_base_url: 'https://www.emag.hu',
    search_page_url: 'https://www.emag.hu/search/mobiltelefonok/',
    next_page_selector: '.listing-panel a.js-change-page.js-next-page',
    products_selector: 'div.card-v2',
    product_scraper: async ($, products) => commonProductScraper('emag', $, products),
    illegal_strings: ['Mobiltelefon'],
    replace_strings: [{ 'Motorola Moto Moto': 'Motorola Moto' }, { ',': ' ' }, { 'Újracsomagolt:': '' }]
  },
  gigatron: {
    page_name: 'www.gigatron.rs',
    currency: CONSTANTS.CURRENCIES.RSD,
    products_page_url: 'https://gigatron.rs/mobilni-telefoni-i-oprema/mobilni-telefoni',
    products_page_base_url: 'https://gigatron.rs',
    search_page_url: 'https://gigatron.rs/pretraga?pojam=',
    next_page_selector: '',
    products_selector: '#product-grid div[data-id]',
    product_scraper: async ($, products) => commonProductScraper('gigatron', $, products),
    illegal_strings: [],
    replace_strings: []
  },
  tehnomanija: {
    page_name: 'www.tehnomanija.rs',
    currency: CONSTANTS.CURRENCIES.RSD,
    // products_page_url: 'https://www.tehnomanija.rs/c/telefoni/mobilni-telefoni/smart-telefoni-10040201',
    products_page_url: 'https://www.tehnomanija.rs/c/telefoni/mobilni-telefoni/smart-telefoni-10040201?query=:popularity-desc:allCategories:10040201:inStockFlag:true:inStockFlag:false',
    products_page_base_url: 'https://www.tehnomanija.rs',
    search_page_url: 'https://www.tehnomanija.rs/search/',
    next_page_selector: 'lib-custom-pagination a.page.current + a.page',
    products_selector: '.product-list .product',
    product_scraper: async ($, products) => commonProductScraper('tehnomanija', $, products),
    illegal_strings: ['telefon ', 'smart telefon'],
    replace_strings: [{ 'Samsung Smart telefon': 'Samsung Galaxy' }, { 'Smart telefon': '' }]
  }
};

module.exports = {
  scrap_able_pages: ['emag', 'gigatron', 'alza', 'tehnomanija'],
  // scrap_able_pages: ['tehnomanija'],
  scraping_category: 'phone',
  selectors,
  commonProductScraper,
  scraper_config: scraperConfig
};

// gigatron, emag has Model, Termékkód:
// tehnomanija, alza not has any match.

// only possible with comparing their names in an array. like
