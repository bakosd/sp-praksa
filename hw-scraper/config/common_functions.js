/**
 * Removes each char which could brake the new RegExp.
 *
 * @param s {String}
 * @returns {String} The safe string.
 */
const escapeRegExpMatch = function (s) {
  return s.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
};

module.exports = {
  /**
   * Checks that the string is matching fully the searched string.
   *
   * @param {String} match
   * @param {String} str
   */
  matchExact: (match, str) => {
    return new RegExp(`\\b${escapeRegExpMatch(match)}\\b`).test(str);
  }
};
