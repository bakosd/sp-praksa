module.exports = {
  CURRENCIES: {
    HUF: { slug: 'HUF', name: 'Ft', multiplier: 0.0026 },
    RSD: { slug: 'RSD', name: 'RSD', multiplier: 0.0085 }
  },
  CURRENCY_NAMES: ['Ft', 'RSD'],
  CAPACITIES: {
    RAM: [2, 3, 4, 6, 8, 12, 16],
    STORAGE: [1, 2, 3, 4, 6, 8, 12, 16, 32, 64, 128, 256, 512]
  },
  CAPACITY_MAGNITUDE: ['GB', 'TB']

};
