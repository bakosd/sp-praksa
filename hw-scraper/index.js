const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
const { executablePath } = require('puppeteer');
const express = require('express');
const Scraper = require('./modules/scraper');
const ScraperProfiles = require('./config/scrape_profiles');
const fs = require('fs');
const app = express();
const productRoutes = require('./api/routes/products');
const axios = require('axios');

puppeteer.use(StealthPlugin());

app.set('view engine', 'ejs');
app.get('/', async (req, res) => {
  try {
    const productsData = await axios.get('http://localhost:3000/products'); // Use the appropriate URL
    res.render('index', { data: { products: productsData.data.response, shops: ScraperProfiles.scrap_able_pages } });
  } catch (error) {
    console.error('Error fetching product data', error);
    res.render('index', { data: { products: [], shops: ScraperProfiles.scrap_able_pages } });
  }
});

app.use('/products', productRoutes);

/* To begin the scrape process open this in the browser.
It will take like a ~15 min to scrape and sort the products.
*/
app.get('/shops/scrape', async (req, res) => {
  const browser = await puppeteer.launch({ headless: 'new', executablePath: executablePath() });
  const scrapedProducts = await Scraper.scrapeProducts(browser);
  // const scrapedProducts = JSON.parse(fs.readFileSync('./src/scraped_data/fetched_products.json'));
  const possibleDevices = JSON.parse(fs.readFileSync('./src/supported_devices.json'));
  const checkedProducts = await Scraper.checkProducts(scrapedProducts, possibleDevices);
  const sortedProducts = await Scraper.sortProducts(checkedProducts);
  await Scraper.saveProducts(sortedProducts);

  res.render('scrape', { data: { products: sortedProducts } });
});

app.listen(3000, () => {
  console.log('App running on http://localhost:3000');
});
