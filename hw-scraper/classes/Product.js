class Product {
  brand;
  name;
  category;
  stores;
  product_variants;
  constructor (brand, name, category) {
    this.brand = brand;
    this.name = name;
    this.category = category;
    this.product_variants = [];
    this.stores = [];
  }
}

module.exports = Product;
