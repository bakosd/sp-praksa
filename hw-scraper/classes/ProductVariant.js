const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
const { executablePath } = require('puppeteer');
const cheerio = require('cheerio');

// Add the StealthPlugin to be undetected from anti-bot checks.
puppeteer.use(StealthPlugin());

class ProductVariant {
  name;
  price;
  id;
  avatar;
  rating;
  stock;
  description;
  url;
  page;
  ram;
  storage;
  constructor (name, price, id, avatar, rating, stock, description, url, page, ram, storage) {
    this.name = name;
    this.price = price;
    this.id = id;
    this.avatar = avatar;
    this.rating = rating;
    this.stock = stock;
    this.description = description;
    this.url = url;
    this.page = page;
    this.ram = ram;
    this.storage = storage;
  }

  // Not needed?
  async scrapeProductDetails (browser = null) {
    try {
      if (!browser) {
        browser = await puppeteer.launch({ headless: 'new', executablePath: executablePath() });
      }

      const productPage = await browser.newPage();
      await productPage.goto(this.url);

      const productDetailsFetch = await productPage.evaluate(() => {
        return {
          html: document.documentElement.innerHTML
        };
      });

      await productPage.close();

      if (productDetailsFetch !== undefined) {
        return cheerio.load(productDetailsFetch.html);
      } else {
        return null;
      }
    } catch (error) {
      console.error('Error in scrapeProductDetails:', error);
      return null;
    }
  }
}

module.exports = ProductVariant;
