class ApiError extends Error {
  title = 'error';
  status;

  constructor (status, message) {
    super();
    super.message = message;
    this.status = status;
  }
}
module.exports = ApiError;
