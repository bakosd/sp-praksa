const cheerio = require('cheerio');
const ScrapeConfig = require('../config/scrape_profiles');
const CommonFunctions = require('../config/common_functions');
const Product = require('../classes/Product');
const mysql = require('mysql');
const dbConfig = require('../config/database');
const fs = require('fs');

/**
 * Scrapes the products using the ScrapeConfig (./config/scrape_profiles.js)
 *
 * @param browser {puppeteer} A puppeteer browser instance.
 * @returns {Promise<*[Product]>} Returns an array which contains the scraped products in Product objects.
 */
async function scrapeProducts (browser) {
  let productsArray = [];
  let nextButton;
  try {
    console.time('scrapeProducts for all pages');
    for (const page of ScrapeConfig.scrap_able_pages) {
      console.log(`Scraping ${page}...`);
      const currPage = ScrapeConfig.scraper_config[page];
      let firstRun = true;
      let productsCount = 0;

      do {
        const $ = await fetchPage(
          browser,
          firstRun ? currPage.products_page_url : currPage.products_page_base_url + nextButton.attr('href'),
          firstRun
        );
        nextButton = await $(currPage.next_page_selector);
        const products = [].concat(...await currPage.product_scraper($, $(currPage.products_selector)));
        productsArray.push(products);
        productsCount += products.length;

        if (firstRun) {
          firstRun = false;
        }
      }
      while (nextButton.length);
      console.log(page + ' contained ' + productsCount + 'pcs product(product variant) in total.');
    }

    console.timeEnd('scrapeProducts for all pages');
    try {
      productsArray = [].concat(...productsArray);
      fs.writeFile('./src/scraped_data/fetched_products.json', JSON.stringify(productsArray, null, 2), (err) => {
        if (err) {
          console.log(err);
        }
      });
    } catch (error) {
      console.error('Error writing to JSON file:', error);
    }
  } catch (error) {
    console.error(error);
  }
  return productsArray;
}

/**
 * Fetches the page from the url which is provided.
 * In case it's the first page it will scroll down while it can.
 *
 * @param browser {puppeteer} A puppeteer browser instance
 * @param url {String} The url which will be fetched.
 * @param firstPage {boolean} Indicates to scroll down while its possible, or not.
 * @returns {Promise<CheerioAPI>}
 */
const fetchPage = async (browser, url, firstPage) => {
  console.time(url + ' time');
  const page = await browser.newPage();
  await page.goto(url);

  const pageData = await page.evaluate(async (firstPage) => {
    if (firstPage) {
      // eslint-disable-next-line no-async-promise-executor
      await new Promise(async (resolve) => {
        while (innerHeight !== document.documentElement.scrollHeight) {
          // eslint-disable-next-line no-global-assign
          innerHeight = document.documentElement.scrollHeight;
          window.scrollBy(0, window.innerHeight, 'smooth');
          await new Promise((resolve) => setTimeout(resolve, 5000));
        }
        resolve();
      });
    }
    return {
      html: document.documentElement.innerHTML
    };
  }, firstPage);
  await page.close();

  console.timeEnd(url + ' time');
  return cheerio.load(pageData.html);
};

/**
 * Check the products which were fetched, and group them up by the possibleProducts array.
 * It checks that there is no longer possible name for a product, and then adds it.
 * Example: Xiaomi Redmi Note 12 Pro, will not go to Xiaomi Redmi Note 12, because it has an extra 'Pro'.
 *
 * @param products {[Product]} Product instances array
 * @param possibleProducts {[]} The array which contains the possible objects.
 * @returns {Promise<*[Product]>} Returns the array of Products, which are grouped together.
 */
const checkProducts = async (products, possibleProducts) => {
  const similarProducts = [];
  const possibleProductsArray = [];
  console.time('Possible products array took: ');
  for (const possibleProduct of possibleProducts) {
    const brand = possibleProduct.brand;
    const brandAlternative = (possibleProduct.brand_alternative ?? brand).toLowerCase();
    const deviceSlug = possibleProduct.device.toLowerCase();
    const marketingName = possibleProduct.marketing_name.toLowerCase();
    const _marketingName = marketingName.replace(/\s+/g, '');

    if (brand === '' || marketingName === '' || brand === marketingName) {
      continue;
    }
    possibleProductsArray.push({
      brand: brand.toLowerCase(),
      brandOrg: brand,
      brandAlternative,
      deviceSlug,
      marketingName: marketingName.toLowerCase(),
      _marketingName,
      marketingNameOrg: marketingName
    }
    );
  }
  console.timeEnd('Possible products array took: ');
  console.time('Check product occurrences took: ');
  // We loop through each product
  for (const product of products) {
    const productName = product.name.toLowerCase();
    const _ram = product.ram !== 'N/A' ? product.ram : '';
    const _storage = product.storage !== 'N/A' ? product.storage : '';
    const _ramStorage = _ram && _storage ? `${_ram}/${_storage}` : _ram || _storage || '';

    // eslint-disable-next-line no-labels
    outerLoop: for (const pp of possibleProductsArray) {
      if ((
      // Product includes brand OR product includes the alternative and the marketing name.
        (productName.includes(pp.brand) ||
            productName.includes(pp.brandAlternative)) && CommonFunctions.matchExact(pp.marketingName, productName)
      ) ||
      (
        (productName.includes(pp.brand) || productName.includes(pp.brandAlternative)) &&
            (pp.deviceSlug !== pp.brand && pp.deviceSlug !== pp.brandAlternative) && CommonFunctions.matchExact(pp.deviceSlug, productName)
      )) {
        // Checking if there is no longer possible_product_name that match the products name
        for (const longerPossibleProduct of possibleProductsArray) {
          if (
            longerPossibleProduct.brand !== pp.brand ||
              (
                (longerPossibleProduct.brandAlternative && pp.brandAlternative) &&
                  longerPossibleProduct.brandAlternative !== pp.brandAlternative
              )
          ) {
            continue;
          }

          if ((
            CommonFunctions.matchExact(pp.marketingName, longerPossibleProduct.marketingName) &&
              longerPossibleProduct.marketingName.length > pp.marketingName.length
          ) || (
            CommonFunctions.matchExact(pp._marketingName, longerPossibleProduct._marketingName) &&
              longerPossibleProduct._marketingName.length > pp._marketingName.length
          )) {
            // eslint-disable-next-line no-labels
            continue outerLoop;
          }
        }

        const productRealName = (!pp.marketingName.includes(pp.brand))
          ? `${pp.brandOrg} ${pp.marketingNameOrg}`
          : pp.marketingNameOrg;

        const productFullRealName = `${productRealName} ${_ramStorage.trim()}`;

        let productInstance = similarProducts.find(p => (_ramStorage === '' ? CommonFunctions.matchExact(productRealName, p.name) : p.name === productFullRealName));

        if (!productInstance) {
          const possibleInstances = (((_storage.length) && similarProducts.filter(
            p => (CommonFunctions.matchExact(productRealName, p.name) && p.name.includes(_storage) && (p.name.includes(_ram) || !_ram.length))
          ))) || [];

          if (!possibleInstances.length) {
            productInstance = new Product(pp.brand, productFullRealName, ScrapeConfig.scraping_category);
            similarProducts.push(productInstance);
          } else {
            productInstance = possibleInstances[0];
          }
        }

        if (!productInstance.product_variants.find(pv => pv.name === product.name)) {
          productInstance.product_variants.push(product);
        }
        if (!productInstance.stores.find(s => s === product.page)) {
          productInstance.stores.push(product.page);
        }
      }
    }
  }
  console.timeEnd('Check product occurrences took: ');
  fs.writeFile('./src/scraped_data/checked_products.json', JSON.stringify(similarProducts, null, 2), (err) => {
    if (err) {
      console.log(err);
    }
  });
  return similarProducts;
};

/**
 * Sorts the array of products, and gives back those which are in every shop in the ScrapeConfig.
 *
 * @param productsArray {[Product]}
 * @returns {Promise<*[Product]>}
 */
const sortProducts = async (productsArray) => {
  const pages = ScrapeConfig.scrap_able_pages;
  const finalProducts = [];
  const productStoreCount = {};
  const partialProducts = {};

  console.time('Sorting products took: ');
  for (const product of productsArray) {
    const productStoresLength = pages.filter(ps => product.stores.includes(ps)).length;

    if (productStoresLength === pages.length) {
      finalProducts.push(product);
    } else {
      if (!partialProducts[productStoresLength]) {
        partialProducts[productStoresLength] = [];
      }
      partialProducts[productStoresLength].push(product);
    }

    if (!productStoreCount[productStoresLength]) {
      productStoreCount[productStoresLength] = 0;
    }
    productStoreCount[productStoresLength]++;
  }

  for (const key in productStoreCount) {
    console.log('Products which are in ' + key + ' store: ', productStoreCount[key]);
    if (partialProducts[key]) {
      fs.writeFile(`./src/scraped_data/partial_${key}.json`, JSON.stringify(partialProducts[key], null, 2), (err) => {
        if (err) {
          console.log(err);
        }
      });
    }
  }

  console.timeEnd('Sorting products took: ');
  return finalProducts;
};
/**
 * Creates the products instances in the database.
 * @param products {[Product]}
 */
const saveProducts = (products) => {
  const mysqlPool = mysql.createPool(dbConfig.db_config);
  for (const product of products) {
    mysqlPool.getConnection((err, conn) => {
      try {
        if (err) {
          console.error(err);
          conn.release();
          return;
        }
        const productData = { brand: product.brand, name: product.name, category: ScrapeConfig.scraping_category };
        conn.query('INSERT INTO products SET ?', productData, (err, res) => {
          if (!err) {
            console.log(`${product.brand} - ${product.name} has been added`);
            const productId = res.insertId;
            for (const pV of product.product_variants) {
              const productVariantData = {
                id_product: productId,
                name: pV.name,
                store: pV.page,
                price: pV.price,
                initial_id: pV.id,
                avatar: pV.avatar,
                rating: pV.rating,
                stock: pV.stock,
                description: pV.description,
                url: pV.url,
                ram: pV.ram,
                storage: pV.storage
              };
              conn.query('INSERT INTO product_variants SET ?', productVariantData, (err, res) => {
                if (err || !res.insertId) {
                  if (err.code === 'ER_DUP_ENTRY') {
                    console.error(`Already existing product variant: ${pV.name}.`, err);
                  } else {
                    console.error(`Error inserting ${pV.name}.`, err);
                  }
                }
              });
            }
          } else {
            if (err.code === 'ER_DUP_ENTRY') {
              console.error(`Already existing product: ${product.name}.`, err);
            } else {
              console.error(`Error inserting ${product.name}.`, err);
            }
          }
        });
      } catch (e) {
        console.error(e);
      } finally {
        conn.release();
      }
    });
  }
};

module.exports = {
  scrapeProducts,
  checkProducts,
  sortProducts,
  saveProducts
};
