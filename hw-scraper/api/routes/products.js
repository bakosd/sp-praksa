const express = require('express');
const mysql = require('mysql');
const dbConfig = require('../../config/database');
const ApiError = require('../../classes/ApiError');
const router = express.Router();
const mysqlPool = mysql.createPool(dbConfig.db_config);

/**
 * Gives back all the products which match the query and params.
 * Also has a withVariants option, then also gives back all the variants.
 *
 * @param query An SQL query to select the products by
 * @param params The params for the query
 * @param withVariants true || false
 * @returns {Promise<Object || ApiError>}
 */
const getProducts = async (query, params, withVariants = true) => {
  return new Promise((resolve, reject) => {
    mysqlPool.getConnection((err, conn) => {
      try {
        if (err) {
          console.error(err);
          reject(err);
          return;
        }
        conn.query(query, params, async (err, response) => {
          try {
            if (err) {
              console.error(err);
              reject(err);
              return;
            } else if (!response.length) {
              reject(new ApiError(404, 'No data found'));
              return;
            }

            for (const product of response) {
              const productVariants = await getProductVariants('SELECT * FROM product_variants pv WHERE pv.id_product = ?', [product.id_product]);
              product.average_price = (
                productVariants.reduce((prev, curr) => ({ price: prev.price + curr.price })).price / productVariants.length
              ).toFixed(2) + ' €';
              product.best_variant = productVariants.sort((a, b) => {
                return a.price - b.price;
              })[0];
              if (withVariants) {
                product.product_variants = productVariants;
              }
            }

            resolve({
              status: 200,
              data: {
                title: 'success',
                message: 'The products was successfully fetched.',
                response_count: response.length,
                response
              }
            });
          } catch (err) {
            console.log(err);
            reject(err);
          }
        });
      } catch (err) {
        console.error(err);
        reject(err);
      } finally {
        conn?.release();
      }
    });
  });
};
/**
 * Gives back a single product variant, also can give back the parent with the withParent option.
 *
 * @param query An SQL query to select the products by
 * @param params The params for the query
 * @param withParent true || false
 * @returns {Promise<Object || ApiError>}
 */
const getProductVariants = (query, params, withParent = false) => {
  return new Promise((resolve, reject) => {
    mysqlPool.getConnection((err, conn) => {
      try {
        if (err) {
          console.error(err);
          reject(err);
          return;
        }
        conn.query(query, params, async (err, response) => {
          try {
            if (err) {
              console.error(err);
              reject(err);
              return;
            } else if (!response.length) {
              reject(new ApiError(404, 'No data found'));
            }
            // Give back the data in array of response objects if the parent is not needed.
            if (!withParent) {
              resolve(response);
              return;
            }
            // Getting the parent for each product.
            // The parent contains the: brand, model name, avg price and best variant from the product.
            const parents = response.map(async (pV) => {
              const data = await getProducts('SELECT * FROM products WHERE id_product = ?', [pV.id_product], false);
              pV.parent = data.data.response[0];
            });
            await Promise.all(parents);

            resolve({
              status: 200,
              data: {
                title: 'success',
                message: 'The products was successfully fetched.',
                response_count: response.length,
                response
              }
            });
          } catch (err) {
            console.error(err);
          }
        });
      } catch (err) {
        console.error(err);
        reject(err);
      } finally {
        conn?.release();
      }
    });
  });
};

router.get('/', async (req, res, next) => {
  getProducts(
    'SELECT p.id_product, p.brand, p.name, p.category FROM products p',
    []
  ).then((result) => {
    res.status(result.status).json(result.data);
  }).catch((err) => {
    console.log(err);
    res.status(err.status).json(err);
  });
});

router.get('/:idProduct', async (req, res, next) => {
  const idProduct = req.params.idProduct;
  getProducts(
    'SELECT p.id_product, p.brand, p.name, p.category FROM products p WHERE p.id_product = ?',
    [idProduct]
  ).then((result) => {
    res.status(result.status).json(result.data);
  }).catch((err) => {
    console.log(err);
    res.status(err.status).json(err);
  });
});

router.get('/search/:name&:shop&:priceLimit', async (req, res, next) => {
  const queryName = req.params.name?.toLowerCase();
  const queryShop = req.params.shop?.toLowerCase();
  const queryPriceLimit = req.params.priceLimit?.toLowerCase();
  getProductVariants(
    'SELECT * FROM product_variants pv WHERE LOWER(pv.name) LIKE ? AND LOWER(pv.store) LIKE ? AND pv.price < ?',
    [`%${queryName}%`, `%${queryShop}%`, `${queryPriceLimit}`],
    true
  ).then((result) => {
    res.status(result.status).json(result.data);
  }).catch((err) => {
    console.log(err);
    res.status(err.status).json(err);
  });
});

router.get('/search/:name&:shop', async (req, res, next) => {
  const queryName = req.params.name?.toLowerCase();
  const queryShop = req.params.shop?.toLowerCase();
  getProductVariants(
    'SELECT * FROM product_variants pv WHERE LOWER(pv.name) LIKE ? AND LOWER(pv.store) LIKE ?',
    [`%${queryName}%`, `%${queryShop}%`],
    true
  ).then((result) => {
    res.status(result.status).json(result.data);
  }).catch((err) => {
    console.log(err);
    res.status(err.status).json(err);
  });
});

router.get('/search/:name', async (req, res, next) => {
  const queryString = req.params.name?.toLowerCase();
  getProducts(
    'SELECT p.id_product, p.brand, p.name, p.category FROM products p WHERE LOWER(p.brand) LIKE ? OR LOWER(p.name) LIKE ? OR LOWER(CONCAT(p.brand, " ", p.name)) LIKE ?',
    [`%${queryString}%`, `%${queryString}%`, `%${queryString}%`]
  ).then((result) => {
    res.status(result.status).json(result.data);
  }).catch((err) => {
    console.log(err);
    res.status(err.status).json(err);
  });
});

module.exports = router;
